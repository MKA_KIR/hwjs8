const textNode = document.createElement('h3');
textNode.innerHTML = "<strong>Price</strong>";
document.body.prepend(textNode);

const input = document.createElement('input');
textNode.after(input);

const errorMsg = document.createElement("p");
errorMsg.id = 'price-error';
errorMsg.style.color = "red";
input.after(errorMsg);

input.addEventListener("focus", function () {
    this.style.border = "2px solid green";
    this.style.background = "none";
    if(errorMsg.textContent) {
        errorMsg.textContent = "";
    }
});
input.addEventListener("blur", function () {
    const value = +this.value;

        if (value<=0 || !value || isNaN(value)) {
            errorMsg.textContent = 'Please enter correct price';
            this.style.border = "2px solid red";
            this.style.background = "red";
        }
        else {
            const inputSpan = document.createElement('span');
            this.before(inputSpan);
            inputSpan.id = 'spanPrice';
            inputSpan.textContent = `Текущая цена: ${this.value}`;
            const button = document.createElement('button');
            inputSpan.appendChild(button);
            button.innerHTML = `X`;
            this.style.background = 'green';
            errorMsg.textContent = "";
            const thisInput = this;
            button.addEventListener('click', function () {
                const span = document.getElementById('spanPrice');
                span.remove();
                thisInput.style.background = "none";
                thisInput.value = "";
                thisInput.style.border = "";
            });
        }
});